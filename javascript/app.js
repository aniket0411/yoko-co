window.$ = window.jQuery = $;

/*================ Vendors ================*/
require("./vendors/lazy-load");

/*================ Sections ================*/
require("./sectoins/slider");

/*================ Sliders ================*/
require("./sectoins/announcementSlider");
require("./sectoins/productSider");
require("./sectoins/blogSlider");
require("./sectoins/collectionSlider");

// require("../scss/app.scss");
