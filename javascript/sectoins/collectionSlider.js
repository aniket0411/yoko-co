/*------------- CollectionSlider ------------------*/
class CollectionSlider {
  constructor(
    arrows,
    dots,
    infinite,
    slidesToShow,
    slidesToScroll,
    autoplay,
    autoplaySpeed,
    fade
  ) {
    this.arrows = arrows;
    this.dots = dots;
    this.infinite = infinite;
    this.slidesToShow = slidesToShow;
    this.slidesToScroll = slidesToScroll;
    this.autoplay = autoplay;
    this.autoplaySpeed = autoplaySpeed;
    this.fade = fade;

    /* Call init method here like this */
    this.init();
  }

  /* Make 1 Init method like this */
  init = () => {
    $(".slider-for").slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      infinite: true,
      asNavFor: ".slider-nav",
    });
    $(".slider-nav").slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: ".slider-for",
      dots: true,
      centerMode: true,
      focusOnSelect: true,
      infinite: true,
    });
  };
}

$(document).ready(function () {
  /* Call the class contructor method like this by initializing the class */
  new CollectionSlider(false, false, true, 1, 1, true, 1500, true);
});
