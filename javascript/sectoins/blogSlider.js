/*------------- BlogSlider ------------------*/
class BlogSlider {
  constructor(
    arrows,
    dots,
    infinite,
    slidesToShow,
    slidesToScroll,
    autoplay,
    autoplaySpeed,
    fade,
    cssEase
  ) {
    this.arrows = arrows;
    this.dots = dots;
    this.infinite = infinite;
    this.slidesToShow = slidesToShow;
    this.slidesToScroll = slidesToScroll;
    this.autoplay = autoplay;
    this.autoplaySpeed = autoplaySpeed;
    this.fade = fade;
    this.cssEase = cssEase;

    /* Call init method here like this */
    this.init();
  }

  /* Make 1 Init method like this */
  init = () => {
    $(".blogs").slick({
      arrows: this.arrows,
      dots: this.dots,
      infinite: this.infinite,
      slidesToShow: this.slidesToShow,
      slidesToScroll: this.slidesToScroll,
      autoplay: this.autoplay,
      autoplaySpeed: this.autoplaySpeed,
      fade: this.fade,
      cssEase: this.cssEase,
    });
  };
}

$(document).ready(function () {
  /* Call the class contructor method like this by initializing the class */
  new BlogSlider(true, false, true, 1, 1, true, 1500, true, "ease");
});
